'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    auth_url: '@@auth_url',
    api_url: '@@api_url'
  });
