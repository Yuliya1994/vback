'use strict';

angular.module('vacationApp').controller('ListCtrl', ['$scope', '$rootScope', 'MailService', 'VacationService', function ($scope, $rootScope, MailService, VacationService) {

    $scope.vacations = null;
    $scope.prev = '';
    $scope.next = '';
    VacationService.getVacations().success(function (data) {

        $scope.vacations = data;
        $scope.prev = (data.previous != null) ? data.previous : '';
        $scope.next = (data.next != null) ? data.next : '';
        $scope.loaded = true;
    })
        .error(function (err) {
            throw new Error(err);
        });

    $scope.showState = function (num) {
        var states = [];

        states[1] = 'Рассматривается';

        states[20] = 'Одобрена';
        states[21] = 'Отклонена';

        states[30] = 'Подтверждена';
        states[31] = 'Отказ';
        return states[num];
    };

    $scope.page = function (type) {
        $scope.$emit('page');
        if (type == 'next') {
            VacationService.page($scope.next).success(function (data, status) {
                $scope.vacations = data;
                $scope.prev = (data.previous != null) ? data.previous : '';
                $scope.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        } else if (type == 'prev') {
            VacationService.page($scope.prev).success(function (data, status) {
                $scope.vacations = data;
                $scope.prev = (data.previous != null) ? data.previous : '';
                $scope.next = (data.next != null) ? data.next : '';
            }).error(function (err) {
                throw err;
            });
        }
        return false;
    }

}]);