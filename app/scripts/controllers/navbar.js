'use strict';

angular.module('vacationApp').controller('NavBarCtrl', function ($scope, $rootScope, $location, $anchorScroll, AuthorizationService) {
    $scope.userName = AuthorizationService.userName;
    $scope.displayMap = $rootScope.displayMap;

    $rootScope.mobileNavigation = {
        'states': ['list', 'barometer', 'topics', 'barchart', 'timeline'],
        'curr_position': 0
    };


    var isMobile = navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/);
    if (isMobile) {
        angular.element('.navbar-fixed-top').autoHidingNavbar();
    }

    $scope.toggleTheme = function ($event) {
        if ($rootScope.theme == 'day') {
            angular.element('body').removeClass('day').addClass('night');
            $rootScope.theme = 'night';
        } else {
            angular.element('body').removeClass('night').addClass('day');
            $rootScope.theme = 'day';
        }
    };

    $scope.goto = function ($event, action) {
        angular.element($event.currentTarget).addClass('active');

        if (action == 'next') {
            if ($rootScope.mobileNavigation.curr_position == $rootScope.mobileNavigation.states.length - 1) {
                $rootScope.mobileNavigation.curr_position = 0
                action = 'prev';
            } else {
                $rootScope.mobileNavigation.curr_position += 1;
            }

        } else if (action == 'prev') {
            if ($rootScope.mobileNavigation.curr_position == 0) {
                $rootScope.mobileNavigation.curr_position = $rootScope.mobileNavigation.states.length - 1;
                action = 'next';
            } else {
                $rootScope.mobileNavigation.curr_position -= 1;
            }
        }

        var target = $rootScope.mobileNavigation.states[$rootScope.mobileNavigation.curr_position];
        var offset = angular.element('.navbar').height();
        offset = (action == 'prev') ? offset : offset - 50;
        angular.element("body").animate({ scrollTop: angular.element('#' + target).offset().top - offset }, "normal");

        setTimeout(function () {
            angular.element($event.currentTarget).removeClass('active');
        }, 700);
    };

    $scope.logout = function () {
        AuthorizationService.logout();
    };
});
