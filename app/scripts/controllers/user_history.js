'use strict';

angular.module('vacationApp').controller('UserHistoryCtrl', ['$scope', '$rootScope', 'UsersService', 'VacationService', function ($scope, $rootScope, UserService, VacationService) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.user = user;
    $scope.userHistory = null;
    function getHistory() {
        $scope.$emit('getHistory');
        VacationService.getVacationsByUser($scope.user.id)
            .success(function (data, status) {
                $scope.userHistory = angular.fromJson(data);

            })
            .error(function (err, status) {
                throw new Error(err);
            });
    }

    getHistory();

    $scope.defineRangeFromData = function (days, month, year) {
        return VacationService.defineRangeFromData(days, month, year);
    };

    $scope.showState = function (num) {
        var states = [];

        states[1] = 'Рассматривается';

        states[20] = 'Одобрена';
        states[21] = 'Отклонена';

        states[30] = 'Подтверждена';
        states[31] = 'Отказ';
        return states[num];
    };

}]);