'use strict';

angular.module('vacationApp')
    .controller('ConfigurationCtrl', ['$scope', '$rootScope', '$routeParams', 'AuthorizationService', function ($scope, $rootScope, $routeParams, AuthorizationService) {
        $scope.userName = AuthorizationService.userName;
        $scope.languages = $rootScope.languages;
        $scope.selectedLanguages = $rootScope.languages;
    }]
    );
