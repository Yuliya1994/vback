angular.module('vacationApp').controller('UserProfileCtrl', ['$scope', '$rootScope', 'UsersService', 'ngDialog', function ($scope, $rootScope, UsersService, ngDialog) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.loaded = false;
    $scope.user = user;
    console.log( $scope.loaded);
    $scope.first_name = user.first_name;
    $scope.last_name = user.last_name;
    $scope.rank = user.rank;
    $scope.id = user.id;
    $scope.newEmail = user.email;
    $scope.loaded = true;

    $scope.updateUser = function (isValid) {
        if (isValid) {
            var first_name = $scope.first_name;
            var last_name = $scope.last_name;
            if (UsersService.updateUser($scope.id, {'last_name': last_name, 'email': $scope.newEmail, 'first_name': first_name})) {
                ngDialog.open({
                    template: '<p>Данные успешно обновлены</p> ',
                    plain: true

                });
            }
        }

    };

           

}]);