'use strict';

angular.module('vacationApp').controller('SingupCtrl', function ($scope, $location, $routeParams, UsersService, AuthorizationService) {

    $scope.error = null;

    $scope.registration = function (isValid) {

        if (isValid) {

            UsersService.creatUser($scope.email, $scope.password).error(function (data, status, headers, config) {
                if (data.email) {
                    $scope.error  = data.email.toString();
                    $('#username').addClass('error');
                } else if (data.password) {
                    $scope.error  = data.password.toString();
                }

            });
        }
    };


});

