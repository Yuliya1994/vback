'use strict';

angular.module('vacationApp').controller('UserMainCtrl', ['$scope', '$location', '$routeParams', 'UsersService', '$cookies', function ($scope, $location, $routeParams, UsersService, $cookies) {

    function checkUser() {

        if (!$cookies.get('token')) {

            $location.path('#/login/');
        }

    }

    checkUser();

}]);