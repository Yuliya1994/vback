'use strict';

angular.module('vacationApp').controller('AdminHeaderCtrl', ['$scope', '$location', '$routeParams', 'UsersService', '$cookies', 'AuthorizationService', function ($scope, $location, $routeParams, UsersService, $cookies, AuthorizationService) {

    if ($cookies.get('token')) {
        var user = JSON.parse(localStorage.getItem('user'));
        $scope.user = user;
        UsersService.getCurrentUser(user.id);
    } else {
        $location.path('#/login/');
    }

    $scope.logout = function () {

        AuthorizationService.logout();

    }

}]);