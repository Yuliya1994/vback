'use strict';

angular.module('vacationApp').filter('monthName', [function() {
    return function (monthNumber) {
        var monthNames = [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
        return monthNames[monthNumber-1];
    }
}]);

angular.module('vacationApp').filter('dayName', [function() {
    return function (monthNumber) {
        var monthNames = [ 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс' ];

        return monthNames[monthNumber];
    }
}]);

angular.module('vacationApp').filter('addZeroToMontNum', [function(){
    return function(monthNumber){
        if(monthNumber < 10) {
            return 0 + '' + monthNumber;
        }

        return monthNumber;
    }
}]);
angular.module('vacationApp').filter('range', function() {
    return function(input, start, end) {
        start = parseInt(start);
        end = parseInt(end);
        var direction = (start <= end) ? 1 : -1;
        while (start != end) {
            input.push(start);
            start += direction;
        }
        return input;
    };
});
angular.module('vacationApp').filter('rank', [function() {
    return function(rankNum) {
        var rank_list = [
            'Разработчик',
            'Менеджер',
            'Начальник'
        ];

        return rank_list[rankNum];
    }

}]);