'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    auth_url: 'http://light-it-02.tk/api/api-token-auth/',
    api_url: 'http://light-it-02.tk/api/'
  });
