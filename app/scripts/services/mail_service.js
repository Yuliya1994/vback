'use strict';

angular.module('vacationApp').service('MailService', ['$http','configuration','$cookies', function($http, configuration ,$cookies) {
    this.getMailList = function() {
        setAuthHeader($cookies.get('token'));
        return $http.get(configuration.api_url + 'mails/');
    };

    this.addNewMail = function(mail) {
        setAuthHeader($cookies.get('token'));
        return $http.post(configuration.api_url + 'mails/', mail);
    };

    this.updateMail = function(id, mail) {
        setAuthHeader($cookies.get('token'));
        return $http.put(configuration.api_url + 'mails/'+id+'/', {state: mail});
    };

    this.updateMailAction = function(id, action, mail) {
        setAuthHeader($cookies.get('token'));
        var data = {};
        data[action] = true;

        return $http.put(configuration.api_url + 'mails/'+id+'/', data);
    };

    this.deleteMail = function(id) {
        setAuthHeader($cookies.get('token'));
        return $http.delete(configuration.api_url + 'mails/'+id+'/');
    };

    var setAuthHeader = function (token) {
        $http.defaults.headers.common.Authorization = 'Token ' + token;
    };
}]);