'use strict';

angular.module('vacationApp').service('UsersService', ['$http', 'configuration', '$cookies', '$location', 'AuthorizationService', function ($http, configuration, $cookies, $location, AuthorizationService) {

    this.creatUser = function (email, password) {
        var params = {
            "email": email,
            "password": password
        }
       return $http.post(configuration.api_url + 'users/', params)
            .success(function (data) {
                var user_data = data;
                $http.post(configuration.auth_url,
                    $.param({username: email, password: password }),
                    {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
                    .success(function (data) {
                        $cookies.put('token', data.token);
                        localStorage.setItem('user', JSON.stringify(user_data));
                        AuthorizationService.initSession(data.token);
                    });
            });
    };

    this.getCurrentUser = function (id) {

        $http.get(configuration.api_url + 'users/' + id + '/')
            .success(function (data) {
                setAuthHeader($cookies.get('token'));
            }).error(function (data, status, headers, config) {

            });
    };
    this.getUser = function (id) {
        return $http.get(configuration.api_url + 'users/' + id + '/');
    };
    this.updateUser = function (id, data) {
        $http.put(configuration.api_url + 'users/' + id + '/',
            {last_name: data.last_name, first_name: data.first_name, email: data.email})
            .success(function (data) {
                localStorage.removeItem('user');
                localStorage.setItem('user', JSON.stringify(data));
                setAuthHeader($cookies.get('token'));
            });
            return true;

    };
    this.getUsers = function () {
        setAuthHeader($cookies.get('token'));
        return $http.get(configuration.api_url + 'users/');
    };
    this.deleteUser = function (id) {
        setAuthHeader($cookies.get('token'));
        return $http.delete(configuration.api_url + 'users/' + id + '/');
    };
    this.page = function (page) {
        setAuthHeader($cookies.get('token'));
        return $http.get(page);
    };
    var setAuthHeader = function (token) {
        $http.defaults.headers.common.Authorization = 'Token ' + token;
    };

}]);

